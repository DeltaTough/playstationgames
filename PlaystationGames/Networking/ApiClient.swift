//
//  ApiClient.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 14/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

class ApiClient {
    
    let session: NetworkSession
    var baseURL = "https://games.directory/api/v1/play_station/"
    var resourceURL: URL
    
    var path: String? {
        didSet {
            if let path = path,
                let url = URL(string: (baseURL + path)) {
                resourceURL = url
            }
        }
    }
    
    convenience init(session: NetworkSession) {
        self.init(session: session, resourcePath: nil)
    }
    
    init(session: NetworkSession, resourcePath: String?) {
        self.session = session
        
        guard let resourceURL = URL(string: baseURL) else {
            fatalError()
        }
        if let resourcePath = resourcePath {
            self.resourceURL = resourceURL.appendingPathComponent(resourcePath)
        } else {
            self.resourceURL = resourceURL
        }
    }
    
    func fetchResource<T>(completion: @escaping (Result<T, ApiError>) -> Void) where T: Decodable {
        let dataTask = session.dataTask(with: resourceURL) { data, reponse, error in
            if let _ = error as? URLError {
                return completion(.failure(.couldNotConnectToHost))
            }
            guard let jsonData = data else {
                completion(.failure(.jsonError))
                return
            }
            do {
                let jsonDecoder = JSONDecoder()
                let resource = try jsonDecoder.decode(T.self, from: jsonData)
                completion(.success(resource))
            } catch {
                let bodyString = String(data: data!, encoding: .utf8)
                print("error \(String(describing: bodyString))")
                completion(.failure(.serverError))
            }
        }
        dataTask.resume()
    }
    
    func fetchResources<T>(completion: @escaping (Result<[T], ApiError>) -> Void) where T: Decodable {
        let dataTask = session.dataTask(with: resourceURL) { data, response, error in
            if let _ = error as? URLError {
                return completion(.failure(.couldNotConnectToHost))
            }
            guard let jsonData = data else {
                completion(.failure(.jsonError))
                return
            }
            do {
                let jsonDecoder = JSONDecoder()
                let resources = try jsonDecoder.decode([T].self, from: jsonData)
                completion(.success(resources))
            } catch {
                let bodyString = String(data: data!, encoding: .utf8)
                print("error \(String(describing: bodyString))")
                completion(.failure(.serverError))
            }
        }
        dataTask.resume()
    }
}
