//
//  ImageDownloadOperation.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 14/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation
import UIKit

enum ImageSize: String {
    case xs
    case s
    case m
    case l
    case xl
    case xxl
    
    var width: CGFloat {
        switch self {
        case .xs:
            return 300
        case .s:
            return 600
        case .m:
            return 900
        case .l:
            return 1200
        case .xl:
            return 1500
        case .xxl:
            return 2000
        }
    }
    var height: CGFloat {
        switch self {
        case .xs:
            return 300
        case .s:
            return 600
        case .m:
            return 900
        case .l:
            return 1200
        case .xl:
            return 1500
        case .xxl:
            return 2000
        }
    }
}

class ImageDownloadOperation: AsyncOperation {
    var url: URL
    var size: ImageSize
    var resize: Bool
    
    init(url: URL, size: ImageSize, resize: Bool = false) {
        self.url = url
        self.size = size
        self.resize = resize
        super.init()
    }
    
    override func main() {
        guard !isCancelled else { return }
        // local Path
        guard let localPath = ImageDownloadOperation.localFilePath(url: url, size: size) else {
            devPrint("error finding local path: \(url)")
            finish()
            return
        }
        devPrint("localPath: \(localPath)")
        // already downloaded
        if FileManager.default.fileExists(atPath: localPath.absoluteString) {
            devPrint("already downloaded: \(url)")
            finish()
            return
        }
        
        guard let imageData = try? Data(contentsOf: url) else {finish(); return }
        guard let image = UIImage(data: imageData) else {finish(); return }
        if image.size.width <= size.width || image.size.height <= size.height {
            saveImage(localPath: localPath, image: imageData)
            finish()
            return
        }
        if let resizedImage = Toucan(image: image).resize(CGSize(width: size.width, height: size.height), fitMode: Toucan.Resize.FitMode.clip).image, let data = resizedImage.pngData() {
            saveImage(localPath: localPath, image: data)
        } else {
            saveImage(localPath: localPath, image: imageData)
        }
        finish()
    }
    
    
    
    static func getLocalImageDataFor(url: URL, size: ImageSize) -> Data? {
        guard let localFilePath = ImageDownloadOperation.localFilePath(url: url, size: size) else {
            return nil
        }
        return try? Data(contentsOf: localFilePath)
    }
    
    
    static func localFilePath(url: URL, size: ImageSize) -> URL? {
        let docs = try? FileManager.default.url(for:.cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        return docs?.appendingPathComponent("images").appendingPathComponent(size.rawValue).appendingPathComponent(url.path)
    }
    
    internal func saveImage(localPath: URL, image: Data) {
        do {
            try FileManager.default.createDirectory(at: localPath.deletingLastPathComponent(), withIntermediateDirectories: true, attributes: nil)
            try image.write(to: localPath)
        } catch {
            devPrint("ERROR SAVING IMAGE: \(error.localizedDescription)")
        }
    }
    
}


