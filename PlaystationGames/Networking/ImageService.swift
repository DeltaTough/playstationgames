//
//  ImageService.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 14/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation
import UIKit

enum ImageServiceError: Error {
    case invalidData
}

class ImageService: NSObject  {
    let queue = OperationQueue()
    
    static var shared = ImageService()
    private override init() {
        super.init()
        queue.qualityOfService = .background
        queue.maxConcurrentOperationCount = 4
        queue.addObserver(self, forKeyPath: "operations", options: .new, context: nil)
    }
    
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if let object = object as? OperationQueue, object == queue, let keyPath = keyPath, keyPath == "operations" {
            devPrint("ImageService Queue operations: \(queue.operationCount)")
            return
        }
        devPrint("observeValue UNEXPECTED!!! \(String(describing: keyPath)) | \(String(describing: object)) | \(String(describing: change))")
        super.observeValue(forKeyPath: keyPath, of: object, change: change, context: context)
    }
    
    
    func cancelAll() {
        queue.cancelAllOperations()
    }
    
    
    @discardableResult func downloadImage(url: URL, size: ImageSize, completion: @escaping () -> Void) -> ImageDownloadOperation? {
        let op = ImageDownloadOperation(url: url, size: size)
        op.completionBlock = completion
        queue.addOperation(op)
        return op
    }
}


