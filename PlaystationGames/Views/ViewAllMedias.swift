//
//  ViewAllMedias.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 17/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import SwiftUI

struct ViewAllMedias: View {
    @Environment(\.presentationMode) var presentationMode
    
    var medias: [Media]?
    
    var body: some View {
        NavigationView {
            ScrollView {
                ForEach(self.medias!, id: \.self) { media in
                    MediasView(media: media)
                }
            }
            .navigationBarTitle("Medias")
            .navigationBarItems(leading:
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Text("Cancel")
            })
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

