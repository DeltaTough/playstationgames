//
//  GameDetailsView.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 15/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation
import SwiftUI


struct GameDetailsView: View {
    @EnvironmentObject var manager: DetailsViewManager
    var slug: String
    @State private var show_modal_trophy = false
    @State private var show_modal_media = false
    @State private var show_modal_description = false
    
    var body: some View {
        NavigationView {
            ZStack {
                LoadingView(isShowing: .constant(self.manager.isLoading)) {
                    ScrollView(showsIndicators: false) {
                        VStack() {
                            HeaderHeroImage()
                            Section(header: Text("Trophies:")
                                .font(.headline)
                                .padding()) {
                                    VStack {
                                        ForEach(0..<self.manager.trophies.limit(8).count / 2, id: \.self) { row in
                                            HStack {
                                                ForEach(0..<2, id: \.self) { column in
                                                    TrophiesView(trophy: self.manager.trophies[row * 2 + column])
                                                }
                                            }
                                        }
                                    }
                                    ViewAllButton(content: ViewAllTrophies(trophies: self.manager.trophies),
                                                  show_modal: self.$show_modal_trophy)
                            }
                            Section(header: Text("Medias:")
                                .font(.headline)
                                .padding()) {
                                    VStack {
                                        ForEach(0..<self.manager.medias.limit(8).count / 2, id: \.self) { row in
                                            HStack {
                                                ForEach(0..<2, id: \.self) { column in
                                                    MediasView(media: self.manager.medias[row * 2 + column])
                                                }
                                            }
                                        }
                                    }
                                    ViewAllButton(content: ViewAllMedias(medias: self.manager.medias), show_modal: self.$show_modal_media)
                            }
                            Section(header: Text("Description:")
                                .font(.headline)
                                .padding()) {
                                    Text((self.manager.description ?? "").prefix(500) + "...")
                                        .padding()
                                    ViewAllButton(content: ViewAllDescription(description: self.manager.description), show_modal: self.$show_modal_description)
                            }
                        }
                    }
                    
                }.onAppear {
                    self.manager.loadGameDetails(self.slug)
                }
            }
        }
    }
}

struct ViewAllButton<Content: View>: View {
    let content: Content
    @Binding var show_modal: Bool
    
    var body: some View {
        Button(action: {
            self.show_modal = true
        }) {
            Text("Read All")
                .padding(.bottom, 30)
        }.sheet(isPresented: self.$show_modal) {
            self.content
        }
    }
}


struct HeaderHeroImage: View {
    @EnvironmentObject var manager: DetailsViewManager
    
    var body: some View {
        return ZStack {
            VStack {
                Image(systemName: "photo")
                    .resizable()
                    .fetchingRemoteImage(from: self.manager.game?.covers.serviceURL)
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 200, height: 200)
                    .clipShape(Capsule())
            }
        }
    }
}

struct TrophiesView: View {
    var trophy: Trophy
    @EnvironmentObject var manager: DetailsViewManager
    
    var body: some View {
        return ZStack {
            VStack {
                Image(systemName: "photo")
                    .resizable()
                    .fetchingRemoteImage(from: self.trophy.covers.serviceURL)
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 70, height: 70)
                    .clipShape(Capsule())
                Spacer(minLength: 10)
                Text(self.trophy.name)
            }
        }
    }
}

struct MediasView: View {
    var media: Media
    @EnvironmentObject var manager: DetailsViewManager
    
    var body: some View {
        return ZStack {
            VStack {
                Image(systemName: "photo")
                    .resizable()
                    .fetchingRemoteImage(from: self.media.covers.serviceURL)
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 70, height: 70)
                    .padding(.init(top: 10, leading: 0, bottom: 10, trailing: 30))
            }
        }
    }
}

