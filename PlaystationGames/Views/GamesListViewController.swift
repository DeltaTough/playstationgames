//
//  GamesListViewController.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 14/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

import UIKit

class GamesListViewController: UIViewController {
    
    var searchController: UISearchController?
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    let apiClient = ApiClient(session: URLSession.shared, resourcePath: "games")
    
    var games = [Game]()
    
    var paginationCounter = 1
    
    private var searchTerm = ""
    var searchWasCancelled = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.createSearchBar()
        
        self.addObservers()
        
        self.collectionView.register(GameCollectionViewCell.self, forCellWithReuseIdentifier: "GameCell")
        
        self.collectionView.collectionViewLayout = GamesListViewController.createLayout()
        
        self.loadGames()
    }
    
    private func loadMore() {
        apiClient.path = "games?page=\(self.paginationCounter)&limit=50"
        self.loadGames()
    }
    
    private func loadGames() {
        apiClient.fetchResource { [weak self] (result: Result<PlaystationResponse, ApiError>) in
            switch result {
            case .success(let response):
                guard let strongSelf = self else { return }
                strongSelf.games.append(contentsOf: response.games)
                UIView.runOnMainThread { [weak self] in
                    guard let self = self else { return }
                    self.collectionView.reloadData()
                    self.collectionView.isHidden = false
                    self.activityIndicator.isHidden = true
                }
            case .failure(let error):
                print("error getting games \(error)")
                UIView.runOnMainThread { [weak self] in
                    guard let self = self else { return }
                    self.collectionView.reloadData()
                    self.activityIndicator.isHidden = true
                    self.collectionView.isHidden = false
                }
            }
        }
    }
    
    private func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name:UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        guard let userInformation = notification.userInfo else { return }
        let userInfo = userInformation
        if let keyboard = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardFrame = self.view.convert(keyboard, from: nil)
            self.collectionView.contentInset.bottom = keyboardFrame.height
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.collectionView.contentInset.bottom = 0
    }
    
    private func createSearchBar() {
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController?.searchBar.delegate = self
        self.searchController?.delegate = self
        self.navigationItem.searchController = self.searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
    }
    
    
    static private func createLayout() -> UICollectionViewCompositionalLayout {
        let layout = UICollectionViewCompositionalLayout { (sectionNumber, env) -> NSCollectionLayoutSection? in
            let item = NSCollectionLayoutItem(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .fractionalHeight(1)))
            
            let group = NSCollectionLayoutGroup.horizontal(layoutSize: .init(widthDimension: .fractionalWidth(1), heightDimension: .absolute(170)), subitems: [item])
            
            let section = NSCollectionLayoutSection(group: group)
            section.orthogonalScrollingBehavior = .none
            return section
            
        }
        return layout
    }
    
    // do the following if text has changed in searchBar
    @objc func textHasChangedInSearchBar(_ searchBar: UISearchBar) {
        self.activityIndicator.isHidden = false
        self.collectionView.isHidden = true
        self.paginationCounter = 2
        self.searchTerm = searchBar.text ?? ""
        self.games = [Game]()
        self.performSearchWithText(searchBar.text)
    }
    
    private func performSearchWithText(_ searchTerm: String?) {
        if !(searchTerm ?? "").isEmpty {
            apiClient.path = "games?query=\(searchTerm?.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"
        } else {
            apiClient.path = "games/"
        }
        self.loadGames()
    }
}

extension GamesListViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return games.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GameCell", for: indexPath) as? GameCollectionViewCell
        cell?.configure(with: games[indexPath.row])
        return cell ?? GameCollectionViewCell()
    }
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == self.games.count - 1 {
            self.paginationCounter+=1
            self.loadMore()
        }
    }
}

extension GamesListViewController: UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let gameDetailsViewController = GameDetailsViewController()
        gameDetailsViewController.slug = self.games[indexPath.row].slug
        self.navigationController?.pushViewController(gameDetailsViewController, animated: true)
    }
}

//MARK- SearchBarDelegate
extension GamesListViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
                NSObject.cancelPreviousPerformRequests(withTarget: self, selector: #selector(self.textHasChangedInSearchBar(_:)),
                                                       object: searchBar)
        perform(#selector(self.textHasChangedInSearchBar(_:)), with: searchBar, afterDelay: 0.75)
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        if !self.searchTerm.isEmpty {
            searchBar.text = searchTerm
        }
        return true
    }
}

//MARK- SearchControllerDelegate
extension GamesListViewController: UISearchControllerDelegate {
    func willPresentSearchController(_ searchController: UISearchController) {
        searchController.searchBar.becomeFirstResponder()
    }
}
