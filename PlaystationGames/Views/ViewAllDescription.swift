//
//  ViewAllDescription.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 17/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import SwiftUI

struct ViewAllDescription: View {
    @Environment(\.presentationMode) var presentationMode
    
    var description: String?
    
    var body: some View {
        NavigationView {
            ScrollView {
                Text(description ?? "")
                .padding()
            }
            .navigationBarTitle("Description")
            .navigationBarItems(leading:
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Text("Cancel")
            })
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}
