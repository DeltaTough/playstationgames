//
//  ViewAllTrophies.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 17/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import SwiftUI

struct ViewAllTrophies: View {
    @Environment(\.presentationMode) var presentationMode
    
    var trophies: [Trophy]?
    
    var body: some View {
        NavigationView {
            ScrollView {
                ForEach(self.trophies!, id: \.self) { trophy in
                    TrophiesView(trophy: trophy)
                }
            }
            .navigationBarTitle("Trophies")
            .navigationBarItems(leading:
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                }) {
                    Text("Cancel")
            })
        }.navigationViewStyle(StackNavigationViewStyle())
    }
}


//struct Trophies: View {
//    var trophies: [Trophy]?
//
//    var body: some View {
//        return ZStack {
//            List {
//                VStack {
//                    ForEach(0..<self.trophies.count / 2, id: \.self) { row in
//                        HStack {
//                            ForEach(0..<2, id: \.self) { column in
//                                TrophiesView(trophy: self.trophies[row * 2 + column])
//                            }
//                        }
//                    }
//                }
//            }
//        }
//    }
//}
