//
//  GameDetailsViewController.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 16/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

class GameDetailsViewController: UIViewController {
    var slug: String?
    
    override func loadView() {
        super.loadView()
        let gameDetailView = GameDetailsView(slug: slug ?? "").environmentObject(DetailsViewManager())
        self.addSubSwiftUIView(gameDetailView, to: view)
    }
}
