//
//  DetailsViewManager.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 16/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation
import SwiftUI

class DetailsViewManager: ObservableObject {
    @Published var trophies = [Trophy]()
    @Published var medias = [Media]()
    @Published var game: GameDetails?
    @Published var description: String?
    @Published var isLoading = true
    var slug: String?
    
    let apiClient = ApiClient(session: URLSession.shared, resourcePath: "games")
    
   func loadGameDetails(_ slug: String) {
        apiClient.path = "games/\(slug)"
        apiClient.fetchResource { [weak self] (result: Result<GameDetailsResponse, ApiError>) in
            switch result {
            case .success(let response):
                print(response)
                UIView.runOnMainThread { [weak self] in
                    guard let strongSelf = self else { return }
                    strongSelf.game = response.game
                    strongSelf.trophies = response.game.trophies!
                    strongSelf.medias = response.game.medias!
                    strongSelf.description = response.game.description
                    strongSelf.isLoading = false
                }
            case .failure(let error):
                print("error getting games \(error)")
                UIView.runOnMainThread { [weak self] in
                    guard let strongSelf = self else { return }
                    strongSelf.isLoading = false
                    
                }
            }
        }
    }
}
