//
//  LoadingView.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 15/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation
import SwiftUI

struct LoadingView<Content>: View where Content: View {
    
    @Binding var isShowing: Bool
    var content: () -> Content
    
    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .center) {
                self.content()
                    .disabled(self.isShowing)
                    .opacity(self.isShowing ? 0 : 1)
                    .background(Color(UIColor.white))
                VStack {
                    ActivityIndicator(isAnimating: .constant(true), style: .large)
                }
                .opacity(self.isShowing ? 1 : 0)

            }
        }
    }
}

struct ActivityIndicator: UIViewRepresentable {
    @Binding var isAnimating: Bool
    let style: UIActivityIndicatorView.Style
    
    func makeUIView(context: UIViewRepresentableContext<ActivityIndicator>) -> UIActivityIndicatorView {
        return UIActivityIndicatorView(style: style)
    }

    func updateUIView(_ uiView: UIActivityIndicatorView, context: UIViewRepresentableContext<ActivityIndicator>) {
        isAnimating ? uiView.startAnimating() : uiView.stopAnimating()
    }
}

