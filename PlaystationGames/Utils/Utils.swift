//
//  Utils.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 14/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import UIKit

func devPrint<T>(_ toPrint: @autoclosure () -> T) {
    #if DEBUG
    let printValue = toPrint()
    
    // do not display the bubble if there is a starting emoji
    if let printString = printValue as? String, printString.count > 0 {
        let firstChar = printString[printString.startIndex]
        if String(firstChar).rangeOfCharacter(from: NSCharacterSet.alphanumerics) == nil {
            UIView.runOnMainThread {
                print(printString)
            }
            return
        }
    }
    
    // default behaviour - display speech bubble
    UIView.runOnMainThread {
        print("💭 \(printValue)")
    }
    #endif
}

struct App {
    static let interfaceOrientation = UIApplication.shared.windows.first(where: { $0.isKeyWindow })?.windowScene?.interfaceOrientation
}
