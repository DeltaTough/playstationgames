//
//  SelfConfigureCell.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 14/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

protocol SelfConfiguringCell {
    static var reuseIdentifier: String { get }
    func configure(with app: Game)
}
