//
//  GameCollectionViewCell.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 14/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation
import UIKit

class GameCollectionViewCell: UICollectionViewCell, SelfConfiguringCell {
    static let reuseIdentifier: String = "GameCell"

    let name = UILabel()
    let subtitle = UILabel()
    let imageView = UIImageView()
    
    let loadingIndicator = UIActivityIndicatorView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        let separator = UIView(frame: .zero)
        separator.translatesAutoresizingMaskIntoConstraints = false
        separator.backgroundColor = .quaternaryLabel
        
        name.font = UIFont.preferredFont(forTextStyle: .headline)
        name.numberOfLines = 0
        name.textColor = .label

        subtitle.font = UIFont.preferredFont(forTextStyle: .subheadline)
        subtitle.textColor = .secondaryLabel

        imageView.layer.cornerRadius = 15
        imageView.clipsToBounds = true
        imageView.contentMode = .scaleAspectFit
        
        let innerStackView = UIStackView(arrangedSubviews: [name, subtitle])
        innerStackView.axis = .vertical

        let outerStackView = UIStackView(arrangedSubviews: [imageView, innerStackView])
        outerStackView.translatesAutoresizingMaskIntoConstraints = false
        outerStackView.alignment = .center
        outerStackView.spacing = 10
        
        let stack = UIStackView(arrangedSubviews: [outerStackView, separator])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        contentView.addSubview(stack)

        NSLayoutConstraint.activate([
            separator.heightAnchor.constraint(equalToConstant: 1),
            separator.topAnchor.constraint(equalTo: outerStackView.bottomAnchor),
            
            stack.leadingAnchor.constraint(equalToSystemSpacingAfter: contentView.leadingAnchor, multiplier: 1),
            stack.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
            stack.topAnchor.constraint(equalTo: contentView.topAnchor),
            stack.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            imageView.widthAnchor.constraint(equalToConstant: 150),
            imageView.heightAnchor.constraint(equalToConstant: 150)
        ])
        
        loadingIndicator.startAnimating()
        loadingIndicator.style = .large
        contentView.addSubview(loadingIndicator)
        
        NSLayoutConstraint.activate([
            loadingIndicator.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            loadingIndicator.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
        ])
    }

    func configure(with game: Game) {
        name.text = game.name
        subtitle.text = game.platforms.first
        imageView.getCoversImage(imageURL: game.covers.serviceURL)
        hideSpinner(true)
    }
    
    func hideCell(_ hide: Bool) {
        name.isHidden = hide
        subtitle.isHidden = hide
        imageView.isHidden = hide
    }
    
    func hideSpinner(_ show: Bool) {
        loadingIndicator.isHidden = show
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        name.text = nil
        subtitle.text = nil
        imageView.image = nil
    }

    required init?(coder: NSCoder) {
        fatalError("Not happening")
    }
}
