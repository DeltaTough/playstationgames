//
//  UIImageView+Extension.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 14/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {
    func getCoversImage(imageURL: String?) {
        if let imageURL = imageURL, let url = URL(string: imageURL) {
            if let imageData = ImageDownloadOperation.getLocalImageDataFor(url: url, size: .xs) {
                self.image = UIImage(data: imageData)
            } else {
                DispatchQueue.global(qos: .background).async { [weak self] in
                    ImageService.shared.downloadImage(url: url, size: .xs) { [weak self] in
                        UIView.runOnMainThread {  [weak self] in
                            self?.refreshImage(imageURL: url)
                        }
                    }
                }
            }
        }
    }
    
    fileprivate func refreshImage(imageURL: URL) {
        guard let imageData = ImageDownloadOperation.getLocalImageDataFor(url: imageURL, size: .xs) else {
            return
        }
        self.image = UIImage(data: imageData)
    }
}
