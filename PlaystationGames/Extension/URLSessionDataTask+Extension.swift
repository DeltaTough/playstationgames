//
//  URLSessionDataTask+Extension.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 14/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

enum ApiError: Error {
  case notFound
  case serverError
  case requestError
  case jsonError
  case couldNotConnectToHost
}

protocol NetworkTask {
  func resume()
}

extension URLSessionDataTask: NetworkTask {}

protocol NetworkSession {
  func dataTask(with request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> NetworkTask
  
  func dataTask(with url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> NetworkTask
}

extension URLSession: NetworkSession {
  func dataTask(with request: URLRequest, completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> NetworkTask {
    return dataTask(with: request, completionHandler: completion)
  }
  
  func dataTask(with url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) -> NetworkTask {
    return dataTask(with:url, completionHandler: completion)
  }
}
