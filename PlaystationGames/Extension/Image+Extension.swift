//
//  Image+Extension.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 16/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import SwiftUI
import UIKit

extension Image {
    func fetchingRemoteImage(from url: String?) -> some View {
        guard let url = url, let imageURL = URL(string: url) else {
            return AnyView(self)
        }
        return AnyView(ModifiedContent(content: self, modifier: RemoteImageModifier(url: imageURL)))
    }
}

struct RemoteImageModifier: ViewModifier {
    let url: URL
    @State private var fetchedImage: UIImage? = nil

    func body(content: Content) -> some View {
        if let image = fetchedImage {
            return Image(uiImage: image)
                .resizable()
                .eraseToAnyView()
        }

        return content
            .onAppear(perform: fetch)
            .eraseToAnyView()
    }

    private func fetch() {
        if let imageData = ImageDownloadOperation.getLocalImageDataFor(url: url, size: .xs) {
            self.fetchedImage = UIImage(data: imageData)
        } else {
            DispatchQueue.global(qos: .background).async {
                ImageService.shared.downloadImage(url: self.url, size: .xs) {
                    UIView.runOnMainThread {
                        guard let imageData = ImageDownloadOperation.getLocalImageDataFor(url: self.url, size: .xs) else {
                            return
                        }
                        self.fetchedImage = UIImage(data: imageData)
                    }
                }
            }
        }
    }
}

