//
//  View+Extension.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 16/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import SwiftUI

extension View {
  func eraseToAnyView() -> AnyView {
    return AnyView(self)
  }
}
