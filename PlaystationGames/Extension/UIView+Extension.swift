//
//  UIView+Extension.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 14/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import UIKit

extension UIView {
    class func runOnMainThread(block: () -> ())  {
        if Thread.isMainThread {
            block()
        } else {
            DispatchQueue.main.sync {
                block()
            }
        }
    }
    
    class func loadFromNib<T: UIView>() -> T? {
        guard let nibObjects = Bundle.main.loadNibNamed(self.safeNibName, owner: nil, options: nil) else { return nil }
        for object in nibObjects {
            if let result = object as? T {
                return result
            }
        }
        return nil
    }
    
    static var nibIdentifier: String {
        return self.safeNibName
    }
    
    static var nibForClass: UINib {
        return UINib(nibName: self.safeNibName, bundle: nil)
    }
    
    // return the name of the class without prefixes (split string on `.`)
    static private var safeNibName: String {
        let safeNibName = "\(self)"
        let components = safeNibName.split{$0 == "."}.map ( String.init )
        return components.last ?? ""
    }
}
