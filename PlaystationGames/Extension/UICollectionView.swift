//
//  UICollectionView.swift
//  Woodmac
//
//  Created by Dimitrios Tsoumanis on 04/07/2019.
//  Copyright © 2019 Wood Mackenzie. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    func register<T : UICollectionViewCell>(cell: T.Type) {
        register(cell.nibForClass, forCellWithReuseIdentifier: cell.nibIdentifier)
    }
    
    func register<T : UICollectionViewCell>(cells: [T.Type]) {
        cells.forEach { (cell) in
            register(cell: cell)
        }
    }
    
    func register<T : UICollectionReusableView>(header: T.Type) {
        register(T.nibForClass, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: T.nibIdentifier)
    }
    
    func register<T : UICollectionReusableView>(headers: [T.Type]) {
        for header in headers {
            register(header.nibForClass, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: header.nibIdentifier)
        }
    }
    
    func register<T : UICollectionReusableView>(footer: T.Type) {
        register(T.nibForClass, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: T.nibIdentifier)
    }
    
    func register<T : UICollectionReusableView>(footers: [T.Type]) {
        for footer in footers {
            register(footer.nibForClass, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: footer.nibIdentifier)
        }
    }
    
    func dequeue<T : UICollectionViewCell>(cell: T.Type, indexPath: IndexPath) -> T {
        return dequeueReusableCell(withReuseIdentifier: T.nibIdentifier, for: indexPath) as! T
    }
    
    func dequeueSectionHeader<T : UICollectionReusableView>(view: T.Type, indexPath: IndexPath) -> T {
        return dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: T.nibIdentifier, for: indexPath) as! T
    }
    
    func reloadOnMainThread() {
        UIView.runOnMainThread { [weak self] in
            self?.reloadData()
        }
    }
}

