//
//  GameDetails.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 16/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

struct GameDetailsResponse: Decodable {
    var success: Bool
    var game: GameDetails
    
    private enum CodingKeys: String, CodingKey {
        case success = "success"
        case game = "game"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decode(Bool.self, forKey: .success)
        game = try values.decode(GameDetails.self, forKey: .game)
    }
}

// MARK: - WelcomeGame
struct GameDetails: Decodable {
    var id: Int
    var name: String
    var totalTrophies: Int?
    var hasAddons: Bool?
    var hasTrophies: Bool?
    var covers: Covers
    var medias: [Media]?
    var trophies: [Trophy]?
    var description: String

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case description = "description"
        case totalTrophies = "total_trophies"
        case hasAddons = "has_addons"
        case hasTrophies = "has_trophies"
        case covers = "covers"
        case medias = "medias"
        case trophies = "trophies"
        case name = "name"
    }
}


// MARK: - Trophy
struct Trophy: Decodable, Hashable {
    static func == (lhs: Trophy, rhs: Trophy) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    let id: Int
    let name: String
    let covers: Covers

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case covers = "covers"
    }
}

// MARK: - Media
struct Media: Decodable, Hashable {
    static func == (lhs: Media, rhs: Media) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    let id: Int
    let remoteURL: String
    let covers: Covers

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case remoteURL = "remote_url"
        case covers = "covers"
    }
}
