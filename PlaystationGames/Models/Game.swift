//
//  Game.swift
//  PlaystationGames
//
//  Created by Dimitrios Tsoumanis on 14/02/2021.
//  Copyright © 2021 Dimitrios Tsoumanis. All rights reserved.
//

import Foundation

struct PlaystationResponse: Decodable {
    let success: Bool
    let games: [Game]
    let meta: Meta
    
    private enum CodingKeys: String, CodingKey {
        case success = "success"
        case games = "games"
        case meta = "meta"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        success = try values.decode(Bool.self, forKey: .success)
        games = try values.decode([Game].self, forKey: .games)
        meta = try values.decode(Meta.self, forKey: .meta)
    }
}

// MARK: - Game
struct Game: Decodable, Hashable {
    static func == (lhs: Game, rhs: Game) -> Bool {
       return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    var id: Int
    var platforms: [String]
    var name: String
    var covers: Covers
    var slug: String

    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case platforms = "platforms"
        case name = "name"
        case covers = "covers"
        case slug = "slug"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        id = try values.decode(Int.self, forKey: .id)
    
        // 3 - Conditional Decoding
        if let name =  try values.decodeIfPresent(String.self, forKey: .name) {
            self.name = name
        } else {
            self.name = "N/A"
        }
        if let platforms =  try values.decodeIfPresent([String].self, forKey: .platforms) {
            self.platforms = platforms
        } else {
            self.platforms = ["N/A"]
        }
        if let covers =  try values.decodeIfPresent(Covers.self, forKey: .covers) {
            self.covers = covers
        } else {
            self.covers = Covers(serviceURL: "N/A")
        }
        if let slug =  try values.decodeIfPresent(String.self, forKey: .slug) {
            self.slug = slug
        } else {
            self.slug = "N/A"
        }
    }
}

// MARK: - Covers
struct Covers: Decodable {
    var serviceURL: String?

    private enum CodingKeys: String, CodingKey {
        case serviceURL = "service_url"
    }
}

// MARK: - Meta
struct Meta: Decodable {
    let pagination: Pagination
}

// MARK: - Pagination
struct Pagination: Decodable {
    let link: Link
    let currentPage, pageItems, totalPages, totalCount: String

    enum CodingKeys: String, CodingKey {
        case link = "Link"
        case currentPage = "Current-Page"
        case pageItems = "Page-Items"
        case totalPages = "Total-Pages"
        case totalCount = "Total-Count"
    }
}

// MARK: - Link
struct Link: Decodable {
    let first, next, last: String?
}

